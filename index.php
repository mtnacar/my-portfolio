<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<!-- Tells browser on how to control page's dimensions and scaling. -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<!-- Allows web authors to choose what version of IE to render. -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<!-- Provides requirements for Facebook Sharing -->
	<meta property="og:title" content="Matthew Web Developer Portfolio" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="Join me in my Web Development Bootcamp Journey!" />
	<meta property="og:url" content="http://www.tuitt.com" />
	<meta property="og:image" content="https://d3ojsfl21hfxhc.cloudfront.net/link_preview.jpg" />
	
	<!-- Provides the requirements for Twitter Sharing -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="http://www.tuitt.com" />
	<meta name="twitter:title" content="Join me in my Web Development Bootcamp Journey!" />
	<meta name="twitter:description" content="Join me in my Web Development Bootcamp Journey!" />

	<title>Portfolio &#149 Matthew Nacar</title>
	<!-- Font Style -->
	<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
	<!-- Fontawesome Icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<!-- Internal CSS -->
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>

	<canvas width="500" height="200" id="canv"></canvas>

	<header>
		<ul>
			<li><a class="wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1s" href="#home">Home</a></li>
			<li><a class="wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1.1s" href="#about">About</a></li>
			<li><a class="wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1.2s" href="#service">Services</a></li>
			<li><a class="wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1.3s" href="#project">Projects</a></li>
			<li><a class="wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1.4s" href="#contact">Contact</a></li>
		</ul>
	</header>

	<section id="home" class="home">
		<div class="name">
			<h1 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1s">Matthew Nacar</h1>
			<h5 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1.2s">Web Developer</h5>
		</div>
	</section>

	<section id="about" class="about">
		<div class="section-title">
			<h2 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">About</h2>
		</div>
		<div class="about-info">
			<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
				I am a Full Stack Developer who specializes in HTML, CSS, SASS, SCSS, JavaScript, jQuery, Bootstrap, PHP and Laravel.
			</p>
			<a href="Nacar, Matthew.pdf" target="_blank" class="wow fadeInUp downloadbtn shrink-on-hover" data-wow-duration=".5s" data-wow-delay=".7s">Download Resume</a>

			<div class="wow fadeInUp timeline" data-wow-duration=".5s" data-wow-delay=".9s">
				<div class="col">
					<span>2014-2015</span>
					<h6>
						CSR - Convergys
					</h6>
				</div>
				<div class="col">
					<span>2015-2017</span>
					<h6>
						TSR - Telstra by Teletech
					</h6>
				</div>
				<div class="col">
					<span>2017-2018</span>
					<h6>
						Sales Associate - Eastwest Bank
					</h6>
				</div>
				<div class="col">
					<span>2019-2020</span>
					<h6>
						Sales Representative - WNS Global Services
					</h6>
				</div>
			</div>
		</div>
	</section>

	<section id="service" class="service">
		<div class="section-title">
			<h2 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">Services</h2>
		</div>
		<div class="service-info">
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s" src="./assets/images/webdev.jpg">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">Web Development</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">Web development can range from developing a simple single static page of plain text to complex web-based internet applications, electronic businesses, and social network services.</p>
			</div>
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s" src="./assets/images/webmaintain.jpg">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">Web Maintenance</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">Website maintenance should be done on a consistent basis in order to keep your website healthy, encourage continued traffic growth, and strengthen your SEO and Google rankings.</p>
			</div>
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".7s" src="./assets/images/webdesign.png">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".8s">Web Design</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s">Web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization.</p>
			</div>
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".7s" src="./assets/images/ecommerce.png">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".8s">E-commerce</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s">E-commerce is the activity of electronically buying or selling of products on online services or over the Internet. Get the chance to have your own selling platform!</p>
			</div>
		</div>
	</section>

	<!-- <div class="gallery" id="gallery">
		<div class="section-title">
			<h2>Gallery</h2>
		</div>
		<div class="gallery-info">
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
			<div class="col">
				<img src="https://picsum.photos/g/250/150/?random">
			</div>
		</div>
	</div> -->

	<section id="project" class="project">
		<div class="section-title">
			<h2 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">Projects</h2>
		</div>
		<div class="project-info">
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s" src="./assets/images/south-unite.png">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">South Unite</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">Web design and development for a car organization.</p>
				<br>
				<a href="https://mtnacar.gitlab.io/capstone1" target="_blank" class="wow fadeInUp shrink-on-hover" data-wow-duration=".5s" data-wow-delay=".7s">View Project</a>
			</div>
			<div class="col">
				<img class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s" src="./assets/images/ecommerce.png">
				<h3 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">E-commerce Framework</h3>
				<p class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">Manual coded ecommerce website that can be adjusted to client needs.</p>
				<br>
				<a href="" class="wow fadeInUp shrink-on-hover" data-wow-duration=".5s" data-wow-delay=".7s">View Project</a>
			</div>
	</section>



	<?php
		echo '<section id="contact" class="contact">
				<div class="section-title">
					<h2 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">Contact</h2>
				</div>
				<div class="contact-info">
					<form method="POST" action="contact.php">
						<input type="text" class="wow fadeInUp textField" data-wow-duration=".5s" data-wow-delay=".4s" placeholder="Full Name" name="sender" required="">
						<input type="text" class="wow fadeInUp textField" data-wow-duration=".5s" data-wow-delay=".5s" placeholder="Email Address" required="" name=senderEmail>
						<textarea class="wow fadeInUp textField messageField" data-wow-duration=".5s" data-wow-delay=".6s" placeholder="Your Message" name="message"></textarea>
						<input type="submit" name="submit" class="wow fadeInUp textField submitButton" data-wow-duration=".5s" data-wow-delay=".8s" value="Send Message">
					</form>
				</div>
			</section>'
	?>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script type="text/javascript">
	
	new WOW().init();

</script>

<script type="text/javascript">
	const canvas = document.getElementById('canv');
	const ctx = canvas.getContext('2d');
	const w = canvas.width = document.body.offsetWidth;
	const h = canvas.height = document.body.offsetHeight;
	const cols = Math.floor(w/20) + 1;
	const yPos = Array(cols).fill(0);

	ctx.fillStyle = '#000';
	ctx.fillRect(0,0,w,h);

	function Matrix(){
		ctx.fillStyle = '#0001';
		ctx.fillRect(0,0,w,h);

		ctx.fillStyle = '#0f0';
		ctx.font = '15pt monospace';

		yPos.forEach((y,ind) => {
			const text = String.fromCharCode(Math.random() * 128);
			const x = ind * 20;
			ctx.fillText(text,x,y);

			if(y > 100 + Math.random() * 10000) yPos[ind] = 0;
			else yPos[ind] = y + 20;
		})
	}
	setInterval(Matrix, 50);
</script>

</body>
</html>